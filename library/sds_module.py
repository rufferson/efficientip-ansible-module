#!/usr/bin/python
# -*-coding:Utf-8 -*
##########################################################
"""
ansible raw module for EfficientIP SOLIDserver create/add/modify
API functions
"""

from __future__ import absolute_import, division, print_function
import json
from SOLIDserverRest import SOLIDserverRest, SERVICE_MAPPER
from ansible.module_utils.basic import AnsibleModule

ANSIBLE_METADATA = {'status': ['preview'],
                    'supported_by': 'EfficientIP community',
                    'version': '0.2'}

DOCUMENTATION = """
---
module: sds_module
author: 
  - Gregory CUGAT
  - Alex Chauvin
short_description: Link ANSIBLE and Python Lib for SOLIDserver API-REST.
requirements:
  - Python Lib for SOLIDserver: SOLIDserverRest v2.0 and upper
description:
  - module permits to call create/delete DDI functions of SOLIDserver
extends_documentation_fragment: nothing
options:
  sds_host:
    description:
      - SOLIDserver manager IP address or name
    type: str
    required: true
  sds_password:
    description:
      - SOLIDserver api account password
    type: str
    required: true
  sds_user:
    description:
      - SOLIDserver api account name
    type: str
    required: true
  sds_auth_method:
    description:
      - SOLIDserver api authentication method
    type: str
    required: false
    choices:
      - native
      - basicauth
  service:
    description:
      - service to call
    type: str
    required: true
    choices:
      - ip_site_create
      - ip_site_update
      - ip_site_count
      - ip_site_list
      - ip_site_info
      - ip_site_delete
      - ip_subnet_create
      - ip_subnet_update
      - ip_subnet_count
      - ip_subnet_list
      - ip_subnet_info
      - ip_subnet_delete
      - ip_subnet_find_free
      - ip_subnet6_create
      - ip_subnet6_update
      - ip_subnet6_count
      - ip_subnet6_list
      - ip_subnet6_info
      - ip_subnet6_delete
      - ip_subnet6_find_free
      - ip_pool_create
      - ip_pool_update
      - ip_pool_count
      - ip_pool_list
      - ip_pool_info
      - ip_pool_delete
      - ip_pool6_create
      - ip_pool6_update
      - ip_pool6_count
      - ip_pool6_list
      - ip_pool6_info
      - ip_pool6_delete
      - ip_address_create
      - ip_address_update
      - ip_address_count
      - ip_address_list
      - ip_address_info
      - ip_address_delete
      - ip_address_find_free
      - ip_address6_create
      - ip_address6_update
      - ip_address6_count
      - ip_address6_list
      - ip_address6_info
      - ip_address6_delete
      - ip_address6_find_free
      - ip_alias_create
      - ip_alias_update
      - ip_alias_list
      - ip_alias_delete
      - ip_alias6_create
      - ip_alias6_update
      - ip_alias6_list
      - ip_alias6_delete
      - dns_rr_list
      - dns_rr_create
      - dns_rr_update
      - dns_rr_delete
      - dns_server_list
      - app_application_list
      - app_application_create
      - app_application_update
      - app_application_delete
      - app_application_count
      - app_application_info
      - app_pool_create
      - app_pool_update
      - app_pool_list
      - app_pool_count
      - app_pool_info
      - app_pool_delete
      - app_node_create
      - app_node_update
      - app_node_info
      - app_node_count
      - app_node_list
      - app_node_delete
      - app_healthcheck_count
      - app_healthcheck_info
      - app_healthcheck_list
      - member_list
      - dhcp_server_info
      - dhcp_server_count
      - dhcp_server_list
      - dhcp_server6_info
      - dhcp_server6_count
      - dhcp_server6_list
      - dhcp_scope_create
      - dhcp_scope_update
      - dhcp_scope_count
      - dhcp_scope_list
      - dhcp_scope_info
      - dhcp_scope_delete
      - dhcp_scope6_create
      - dhcp_scope6_update
      - dhcp_scope6_count
      - dhcp_scope6_list
      - dhcp_scope6_info
      - dhcp_scope6_delete
      - dhcp_shared_network_create
      - dhcp_shared_network_update
      - dhcp_shared_network_count
      - dhcp_shared_network_list
      - dhcp_shared_network_info
      - dhcp_range_create
      - dhcp_range_update
      - dhcp_range_list
      - dhcp_range_info
      - dhcp_range_count
      - dhcp_range_delete
      - dhcp_range6_create
      - dhcp_range6_update
      - dhcp_range6_list
      - dhcp_range6_info
      - dhcp_range6_count
      - dhcp_range6_delete
      - dhcp_static_create
      - dhcp_static_update
      - dhcp_static_list
      - dhcp_static_info
      - dhcp_static_count
      - dhcp_static_delete
      - dhcp_static6_create
      - dhcp_static6_update
      - dhcp_static6_list
      - dhcp_static6_info
      - dhcp_static6_count
      - dhcp_static6_delete
      - host_device_create
      - host_device_update
      - host_device_delete
      - host_device_list
      - host_device_count
      - host_device_info
      - host_iface_create
      - host_iface_update
      - host_iface_delete
      - host_iface_list
      - host_iface_count
      - host_iface_info
      - host_link_create
      - host_link_update
      - host_link_delete
      - host_link_count
      - host_link_list
      - host_link_info

"""

__metaclass__ = type


# --------------------------------------------------------
def main():
    """ main function for ansible module """
    module_args = dict(
        sds_host=dict(required=True),
        sds_user=dict(required=True),
        sds_password=dict(required=True, no_log=True),
        sds_auth_method=dict(required=False),
        service=dict(required=True),
        params=dict(required=True, type='dict')
    )

    result = dict(
        changed=False,
    )

    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    # get variables from playbook
    sds_host = module.params['sds_host']
    sds_user = module.params['sds_user']
    sds_pass = module.params['sds_password']
    sds_service = module.params['service']
    sds_parameters = module.params['params']
    sds_auth_method = module.params['sds_auth_method']

    result['call_params'] = {
        'sds_host': sds_host,
        'service': sds_service,
        'service_args': sds_parameters
    }

    if sds_service not in SERVICE_MAPPER:
        module.fail_json(msg='service unknown',
                         **result)

    # CONNECTION
    con = SOLIDserverRest(sds_host)
    if sds_auth_method == 'native':
        con.use_native_sds(sds_user, sds_pass)
    else:  # basic
        con.use_basicauth_sds(sds_user, sds_pass)

    con.set_ssl_verify(False)

    if module.check_mode:
        module.exit_json(**result)

    #QUERY
    sds_answer = con.query(sds_service, sds_parameters)

    try:
        rjson = sds_answer.json()
    except json.JSONDecodeError:   # pragma: no cover
        module.fail_json(msg='no json in return from SOLIDserver',
                         **result)

    for label in ['errno',
                  'errmsg',
                  'severity',
                  'category',
                  'ip_name',
                  'site_name'
                  'hostaddr',
                  'param_format',
                  'param_value',
                  'ret_oid'
                 ]:
        if label in rjson:
            result[label] = rjson[label]

    result['raw_result'] = rjson

    if sds_answer.status_code == 200:
        result['changed'] = True

    if sds_answer.status_code == 201:
        result['changed'] = True
        result['created'] = True

    if sds_answer.status_code >= 300:
        result['status_code'] = sds_answer.status_code
        module.fail_json(msg='error from SOLIDserver', **result)

    module.exit_json(**result)

main()
