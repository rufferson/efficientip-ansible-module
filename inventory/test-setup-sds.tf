provider "solidserver" {
  host      = var.solidserver_host
  username  = var.solidserver_user
  password  = var.solidserver_password
  sslverify = false
}

resource "solidserver_ip_space" "ansible" {
  name   = "ansible"
}

resource "solidserver_ip_subnet" "block" {
  space            = solidserver_ip_space.ansible.name
  request_ip       = "172.18.128.0"
  prefix_size      = 24
  name             = "block"
  terminal         = false
}

resource "solidserver_ip_subnet" "docker" {
  space            = solidserver_ip_space.ansible.name
  block            = solidserver_ip_subnet.block.name
  prefix_size      = 24
  name             = "docker"
  terminal         = true
  gateway_offset   = 1
}

resource "solidserver_ip_pool" "pool" {
  space            = solidserver_ip_space.ansible.name
  subnet           = solidserver_ip_subnet.docker.name
  name             = "pool"
  #start            = solidserver_ip_subnet.docker.address
  start            = "172.18.128.3"
  size             = 80
}

resource "solidserver_device" "ansible80" {
  count = var.servers80

  name   = "ansible-target-80${count.index}"
}

resource "solidserver_device" "ansible78" {
  count = var.servers78

  name   = "ansible-target-78${count.index}"
}

resource "solidserver_device" "ansible77" {
  count = var.servers77

  name   = "ansible-target-77${count.index}"
}

resource "solidserver_device" "ansible76" {
  count = var.servers76

  name   = "ansible-target-76${count.index}"
}

resource "solidserver_ip_address" "docker80" {
   count = var.servers80

   space   = solidserver_ip_space.ansible.name
   subnet  = solidserver_ip_subnet.docker.name
   pool    = solidserver_ip_pool.pool.name
   name    = "ansible-target-80${count.index}"

   device  = solidserver_device.ansible80[count.index].name
}

resource "solidserver_ip_address" "docker78" {
   count = var.servers78

   space   = solidserver_ip_space.ansible.name
   subnet  = solidserver_ip_subnet.docker.name
   pool    = solidserver_ip_pool.pool.name
   name    = "ansible-target-78${count.index}"

   device  = solidserver_device.ansible78[count.index].name
}

resource "solidserver_ip_address" "docker77" {
   count = var.servers77

   space   = solidserver_ip_space.ansible.name
   subnet  = solidserver_ip_subnet.docker.name
   pool    = solidserver_ip_pool.pool.name
   name    = "ansible-target-77${count.index}"

   device  = solidserver_device.ansible77[count.index].name
}

resource "solidserver_ip_address" "docker76" {
   count = var.servers76

   space   = solidserver_ip_space.ansible.name
   subnet  = solidserver_ip_subnet.docker.name
   pool    = solidserver_ip_pool.pool.name
   name    = "ansible-target-76${count.index}"

   device  = solidserver_device.ansible76[count.index].name
}
