#!/bin/bash

lockdir=/tmp/ansible_ssh_lock

semaphore_wait() {
    while [ true ]
    do
        if mkdir "$lockdir" 2> /dev/null
        then
            return
        else
            echo "wait"
            sleep 1
        fi
    done
}

semaphore_release() {
    rmdir "$lockdir"
}

semaphore_wait

fgrep -v $1 $HOME/.ssh/known_hosts > /tmp/ssh-key-$$
ssh-keyscan 172.18.128.3 $1 >> /tmp/ssh-key-$$
cp /tmp/ssh-key-$$ $HOME/.ssh/known_hosts
rm -f /tmp/ssh-key-$$

semaphore_release
